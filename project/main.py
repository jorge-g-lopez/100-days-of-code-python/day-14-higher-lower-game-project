"""Higher Lower Game Project"""

import random

from art import LOGO, VS
from game_data import DATA


def random_data():
    """Return a random item number of the game data list"""
    return random.randint(0, len(DATA) - 1)


def print_compare_message(prefix, index):
    """Print the compare message using a prefix and the game data index"""
    message = (
        prefix
        + " "
        + DATA[index]["name"]
        + ", a "
        + DATA[index]["description"]
        + ", from "
        + DATA[index]["country"]
        + "."
    )
    print(message)
    # print(f'Debug: follower_count: {DATA[index]["follower_count"]}')


score = 0
correct = True
a = random_data()

print(LOGO)

while correct:
    b = a
    while b == a:
        b = random_data()

    print_compare_message("Compare A:", a)
    print(VS)
    print_compare_message("Against B:", b)
    more = input("Who has more followers? Type 'A' or 'B': ").upper()

    if more == "A":
        higher = a
        lower = b
    else:
        higher = b
        lower = a

    print(LOGO)

    if DATA[higher]["follower_count"] > DATA[lower]["follower_count"]:
        score += 1
        print(f"You're right! Current score: {score}")
        a = higher
    else:
        print(f"Sorry, that's wrong. Final score: {score}")
        correct = False
